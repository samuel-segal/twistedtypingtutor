/**
 * Authored by Dr. Walter Piper
 * Contributed to by Samuel Segal
 * Switches a letter of text input with another
 * Handles majority of web page
 */

var started = false
var startTime
var experimentComplete = false


//Long way of putting everything into array
var text1 = "You miss 100% of the shots you don't take."
var text2 = 'On many long journeys have I gone. And waited, too, for others to return from journeys of their own. Some return; some are broken; some come back so different only their names remain.'
var text3 = 'How did you know, how did you know, Master Yoda? Master Yoda knows these things. His job it is.'
var text4 = 'The more nobly and magnanimously thou conductest thyself, and the less thou vauntest of thy wealth and power.'
var text5 = 'Secret, shall I tell you? Grand Master of Jedi Order am I. Won this job in a raffle I did, think you?'

var experimentTexts = [text1,text2,text3,text4,text5]

//Might look redundant, but allows for greater flexibility.
var switch1 = { 'S': 'S' } //control
var switch2 = { 'S': 'S', 'R': 'R' }
var switch3 = {
	'O': 'I',
	'I': 'O'
}
var switch4 = {
	'E': 'R',
	'R': 'E',
	'T': 'A',
	'A': 'T'
}
var switch5 = {
	'H': 'M',
	'M': 'H',
	'O': 'N',
	'N': 'O',
	'J': 'I',
	'I': 'J'
}

var keySwitches = [switch1, switch2, switch3, switch4, switch5]

var num_E_complete = 0

var consoleArray = new Array()
consoleArray.push('Copy this text and send to experimenter: ')

var isTextFinished = false

function startExperiment() {
	if (!started) {
		$('#targetText').text(experimentTexts[num_E_complete])
		$('#targetText').css({ 'background-color': 'white' })
		$('#typedText').text('')
		started = true
		startTime = new Date()

		$('#hidden').css({ 'position': 'fixed', 'left': '2%', 'width': '5%', 'padding': '0.1%', 'background-color': 'white' })
		var $quit = $('<input type="button" value="QUIT WITHOUT COMPLETING" />')
		$quit.appendTo($("#hidden"))
	}
}

function transformTypedChar(charStr) {
	//A little bit bulky, but it reads pretty clearly.

	let currswitch = keySwitches[num_E_complete]

	if (currswitch[charStr]) {
		return currswitch[charStr]
	}

	let charUpp = charStr.toUpperCase()
	if (currswitch[charUpp]) {
		return currswitch[charUpp].toLowerCase()
	}

	return charStr
}

// MAIN FUNCTION BELOW
$(function () {
	$('#typedText').on('focus', startExperiment)

	$('#typedText').on('keyup', function () {
		if (window.event.which == 8) {
			consoleArray.push(['[', 'BACKSPACE', passedTime + ' ms', ']'])
		}
		if (isTextFinished) {
			$('#typedText').val('')
			isTextFinished = false
		}
	})

	var oldval = ''
	$('#typedText').on('input propertychange', function () {

		let newTyped = this.value.substr(oldval.length)
		if (newTyped.length > 0) {
			let changed = newTyped.split('').map(transformTypedChar).join('')
			oldval = oldval + changed
			this.value = oldval
		}
		else {
			oldval = this.value
		}

		passedTime = new Date() // gets time of keyup
		passedTime = passedTime - startTime // calculates ms

		consoleArray.push(['[', newTyped, passedTime + ' ms', ']']) // save to log variable

		if (experimentTexts[num_E_complete].startsWith(this.value)) {  // if text is accurate and no characters precede
			
			consoleArray.push(['[', 'YAY', ']']) 

			$('#typedText').css({ 'background-color': 'rgb(211,237,199)' })
			if (experimentTexts[num_E_complete] == this.value) { // IF EXAMPLE IS COMPLETE

				consoleArray.push(['[', 'COMPLETE!', ']'])
				num_E_complete++   // incrementally add 1 to num complete

				if (num_E_complete >= 5) {  // experiment complete
					$('#targetText').css({ 'background-color': 'rgb(213, 255, 207)' })
					$('#hidden').css({
						'position': 'fixed',
						'left': '2%', 'width': '20%', 'padding': '1%', 'background-color': 'white'
					})
					var $input = $('<input type="button" value="Copy data log" />')
					$input.appendTo($("#hidden"))
					$('#hidden').html('<a href="https://forms.gle/dfUtzgUNf1yQgdCZ7">Click here, then paste from clipboard (Ctrl + v) into this Google form</a>')

					EXPERIMENT_COMPLETE = true
					// copy to clipboard
					navigator.clipboard.writeText(consoleArray).then(function () {
						window.alert("Copy successful! Paste into Google form.")
					}, function () {
						window.alert("copy failed")
					})
				}

				else {
					$('#targetText').text(experimentTexts[num_E_complete])
					isTextFinished = true
				}

			}
		}
		else {
			consoleArray.push(['[', 'YUCK', ']'])
			$('#typedText').css({ 'background-color': 'rgb(250, 209, 205)' })
		}

	})
	// Log copy function
	$("#hidden").click(function () {
		if (EXPERIMENT_COMPLETE) {
			navigator.clipboard.writeText(consoleArray).then(function () {
				window.alert("Copy successful! Paste into Google form.")
			}, function () {
				window.alert("copy failed")
			})
		}
		else {
			$('#hidden').html('<a href="https://forms.gle/dfUtzgUNf1yQgdCZ7">Click here, then paste from clipboard (Ctrl + v) into this Google form</a>')
			consoleArray.push("[QUIT EARLY]")
			navigator.clipboard.writeText(consoleArray).then(function () {
				window.alert("Copy successful! Paste into Google form.")
			}, function () {
				window.alert("copy failed")
			})
		}
	})
})
// MAIN FUNCTION ABOVE